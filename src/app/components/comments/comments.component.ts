import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import Swal from "sweetalert2";
import {CommentService} from "../../services/comment.service";
import {NgForm} from "@angular/forms";
import {CommentModule} from "../../modules/comment/comment.module";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  title = 'Déjanos tus comentarios'
  commentModule = new CommentModule()
  commentsModules: CommentModule[] = []
  loading = false
  total = 0

  constructor( private commentService: CommentService) { }

  ngOnInit(): void {
    this.loading = true
    this.commentService.getComments().subscribe(resp => {
      this.total = resp.length
      const array1 = resp
      const reversed = array1.reverse()
      // @ts-ignore
      this.commentsModules = reversed
      this.loading = false
    })
  }

  saveComment(form: NgForm) {
    if ( form.invalid ) {
      Object.values( form.controls ).forEach( control => { control.markAllAsTouched() } )
    } else {

      let petition: Observable<any>

      petition = this.commentService.createComment(this.commentModule)
      petition.subscribe(resp => {
        Swal.fire({
          position: "center",
          icon: 'success',
          title: '¡Muchas gracias!',
          text: 'Su comentario fue ingresado correctamente',
          showConfirmButton: true,
          allowOutsideClick: true,
          willClose(popup: HTMLElement) {
            timer: 4000
          }

        })
        window.location.reload()
      })
    }
  }
}
