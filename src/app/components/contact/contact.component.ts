import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import Swal from "sweetalert2";
import {CommentService} from "../../services/comment.service";
import {ContactModule} from "../../modules/contact/contact.module";
import {SubmittedContactus} from "../../interfaces/submitted-contactus";
import {environment} from "../../../environments/environment";
import {ContactUsService} from "../../services/contact-us.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  title2 = 'Contáctenos'
  contactModule = new ContactModule()

  constructor( private commentService: CommentService,
               private contactusService: ContactUsService) {}

  ngOnInit(): void {}

  saveContact(form: NgForm) {
    if ( form.invalid ) {
      Object.values( form.controls ).forEach( control => { control.markAllAsTouched() } )
    } else {

      const newForm: SubmittedContactus = {
        name: '',
        email: '',
        title: '',
        message: '',
        phone: ''
      }

      const urlApi = environment.contactUrlApi

      newForm.name = form.value.name
      newForm.email = form.value.email
      newForm.title = form.value.title
      newForm.message = form.value.message
      newForm.phone = form.value.phone

      if ( this.selectTitle(form) ) {
        if ( this.contactusService.sendContactus(urlApi, newForm).subscribe() ) {
          Swal.fire({
            position: "center",
            icon: 'success',
            title: '¡Gracias por tu mensaje!',
            text: '¡Tu mensaje fue recibido correctamente, nos pondremos en contacto a la brevedad!',
            showConfirmButton: true,
            timer: 4000
          })
        } else {
          Swal.fire({
            position: "center",
            icon: 'error',
            title: '¡No pudimos guardar tu mensaje!',
            text: '¡Por favor, inténtalo nuevamente!',
            showCancelButton: true,
            timer: 4000
          })
        }
      }
    }
  }

  selectTitle( form: NgForm ) {
    if (form.value.title == '1'){
      return false
    } else {
      return true
    }
  }
}
