import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import Swal from "sweetalert2";
import {NgForm} from "@angular/forms";
import {CommentService} from "../../services/comment.service";
import {CommentModule} from "../../modules/comment/comment.module";
import {SubscribeModule} from "../../modules/subscribe/subscribe.module";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  date: Date
  subscribeModule = new SubscribeModule()

  constructor( private commentService: CommentService ) {
    this.date = new Date()
  }

  ngOnInit(): void {
  }

  subscribe(form: NgForm) {
    if ( form.invalid ) {
      Object.values( form.controls ).forEach( control => { control.markAllAsTouched() } )
    } else {

      let petition: Observable<any>

      petition = this.commentService.createSubscribe(this.subscribeModule)
      petition.subscribe(resp => {
        Swal.fire({
          position: "center",
          icon: 'success',
          title: '¡Te has suscrito correctamente!',
          text: '¡Te informaremos de todos nuestros eventos oportunamente!',
          showConfirmButton: true,
          timer: 4000
        })
      })
    }
  }
}
