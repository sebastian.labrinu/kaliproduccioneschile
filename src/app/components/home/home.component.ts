import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {CommentModule} from "../../modules/comment/comment.module";
import {Observable} from "rxjs";
import {CommentService} from "../../services/comment.service";
import Swal from "sweetalert2";

@Component ({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  title = 'Bienvenidos'

  constructor() {}

  ngOnInit(): void {}
}

