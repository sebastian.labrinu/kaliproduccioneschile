export interface SubmittedContactus {
  name: string
  email: string
  title: string
  message: string
  phone: String
}
