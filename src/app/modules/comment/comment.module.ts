import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CommentModule {
  id: string
  name: string
  comment: string

  constructor() {
    this.id = ""
    this.name = ""
    this.comment = ""
  }
}
