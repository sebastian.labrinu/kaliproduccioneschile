import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ContactModule {

  email: string
  name: string
  phone: string
  message: string
  title: string

  constructor() {
    this.email = ''
    this.name = ''
    this.phone = ''
    this.message = ''
    this.title = '1'
  }
}
