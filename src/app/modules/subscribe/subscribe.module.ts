import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SubscribeModule {

  id: string | null
  email: string

  constructor() {
    this.id = ''
    this.email = ''
  }
}
