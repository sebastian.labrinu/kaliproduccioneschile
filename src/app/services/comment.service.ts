import { Injectable } from '@angular/core';
import {CommentModule} from "../modules/comment/comment.module";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {SubscribeModule} from "../modules/subscribe/subscribe.module";
import {ContactModule} from "../modules/contact/contact.module";

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private url = 'https://kaliproduccioneschile-97f64-default-rtdb.firebaseio.com/Comentarios'
  private urlSubscribe = 'https://kaliproduccioneschile-97f64-default-rtdb.firebaseio.com/Suscripciones'
  private urlContact = 'https://kaliproduccioneschile-97f64-default-rtdb.firebaseio.com/Contactos'

  constructor( private http: HttpClient ) { }

  createComment(comment: CommentModule){
    return  this.http.post(`${ this.url }.json`, comment)
      .pipe(map( (resp:any) => { comment.id = resp.name; return comment }))
  }
  createContact(contact: ContactModule){
    return  this.http.post(`${ this.urlContact }.json`, contact)
      .pipe(map( (resp:any) => { return contact }))
  }

  createSubscribe(subscribe: SubscribeModule){
    return  this.http.post(`${ this.urlSubscribe }.json`, subscribe)
      .pipe(map( (resp:any) => { subscribe.id = resp.name; return subscribe }))
  }

  getComments(){
    return this.http.get(`${ this.url }.json`)
      .pipe(
        map( this.createArray)
      )
  }

  private createArray( reserveObj: object ) {
    const comments: CommentModule[] = []
    if ( reserveObj === null ){ return [] }
    Object.keys( reserveObj ).forEach( key => {
      // @ts-ignore
      const reserve: ReserveModule = reserveObj[key]
      reserve.id = key
      comments.push( reserve )
    })
    return comments
  }

}
