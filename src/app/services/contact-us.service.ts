import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SubmittedContactus} from "../interfaces/submitted-contactus";

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor( private http: HttpClient ) {}

  sendContactus(api_url: string, contact: SubmittedContactus) {
    return this.http.post(api_url, contact)
  }
}
